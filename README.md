# How to Run Pilon v1.23

*Note: The following steps were performed in a computing cluster with a Red Hat Linux environment.

## Data

Pilon only takes data that is a the whole genome/scaffolds and the reads that are being aligned to that genome. Pilon takes your whole genome as a FASTA and your reads as an aligned BAM file.

## Preparing the Data

**Aligning with Bowtie2**

The initial step is to create the aligned bam file from your raw reads. To create the aligned file I used Bowtie2 v2.2.9. 

```bowtie2-build -f genome.fasta aligned```

* Genome.fasta, this is the whole genome file or the scaffolds that are being aligned to.
* 'aligned' is the prefix on the index files that will be created.

```bowtie2 -f -x aligned raw_reads.fasta -S output.sam``` 

* -x, is the prefix for the index files of the genome/scaffolds
* -U, unaligned reads
* -S output in SAM format

**Converting SAM to BAM**

From the SAM file you would need to convert to BAM, sorted the file, and then index (the index file is necessary for Pilon to work). To do this I used samtools v1.6.

```samtools view -S -b aligned_output.sam > aligned_output.bam```

```samtools sort aligned_output.bam -o sorted_output.bam```

```samtools index sorted_output.bam```

## Running Pilon

```java -jar pilon-1.23.jar --genome genome.fasta --bam sorted_output.bam --output pilon.out```

The results will be in a fasta file.

**Example Results:**

Pilon Stats

Pilon version 1.23 Mon Nov 26 16:04:05 2018 -0500

Genome: sample_data/Pilon_Whole_Sample.fasta
Fixing snps, indels, gaps, local

Input genome size: 14588
Scanning BAMs
sample_data/Pilon_Sorted_Sample.bam: 502 reads, 0 filtered, 449 mapped, 0 proper, 0 stray, Unpaired 100% 288+/-35, max 10000 unpaired

Processing MK896865.1:1-14588
unpaired sample_data/Pilon_Sorted_Sample.bam: coverage 8

Total Reads: 449, Coverage: 8, minDepth: 5
Confirmed 3232 of 14588 bases (22.16%)
Corrected 8 snps; 1 ambiguous bases; corrected 3 small insertions totaling 4 bases, 0 small deletions totaling 0 bases

* Attempting to fix local continuity breaks
* fix break: MK896865.1:2653-7116 0 -0 +0 NoSolution
* fix break: MK896865.1:7422-7916 0 -0 +0 NoSolution
* fix break: MK896865.1:8251-14227 0 -0 +0 NoSolution

MK896865.1:1-14588 log:
Finished processing MK896865.1:1-14588
Writing updated MK896865.1_pilon to pilon.out.fasta
Mean unpaired coverage: 8
Mean total coverage: 8